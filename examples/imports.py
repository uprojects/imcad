from imcad import *

with display():
    imports("examples/V20X_profile.iges")

with display():
    imports("examples/V20X_profile.step")

with display():
    imports("examples/V20X_profile.stl")
