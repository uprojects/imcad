"""V20X profile example"""

from imcad import *

from imcad.lib.profile import ExtrudedProfile, V20X20

@cached
def demo(count=4, angle=60, length=50, offset=30):
    """Generate rotated profiles set"""
    with union():
        for i in range(0, count):
            with rotate(angle * i), translate(x=offset):
                ExtrudedProfile(V20X20, length).body()

with display(), \
     render("examples/V20X_profile.png", size=(1280, 720)), \
     export("examples/V20X_profile.step"), \
     export("examples/V20X_profile.iges"), \
     export("examples/V20X_profile.stl"), \
     export("examples/V20X_profile.x3d"):
    demo()
