"""Boolean operations example"""

from imcad import *

def elements():
    with extrude(l=3):
        circle(r=1)
    with extrude(l=2):
        circle(d=1, y=-1)

with display(), union():
    with translate(x=0), union():
        elements()
    with translate(x=3), difference():
        elements()
    with translate(x=6), intersection():
        elements()
