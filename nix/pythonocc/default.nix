{
  stdenv, pythonPackages, fetchgit, opencascade, swig, cmake, pkgconfig, 
  libglvnd, libGL, libGLU, libX11, freetype, useCadQueryFork ? false, 
  ... 
}:
pythonPackages.toPythonModule (stdenv.mkDerivation rec {
  pname = "pythonocc-core";
  version = "0.18.2-pre";

  src = fetchgit (if useCadQueryFork then {
    url = "https://github.com/CadQuery/pythonocc-core";
    rev = "454088c";
    sha256 = "0axb3r2cn3v1cmi0805y50k099lcw5adilmncp15fv4lfb94jm3d";
  } else {
    url = "https://github.com/tpaviot/pythonocc-core";
    rev = "595b0a4";
    sha256 = "1fhrivhaw13lkzxdr1lyqjnddg1j9lv3cf0h86avnfbd0zg0jbl5";
  });
  nativeBuildInputs = [ swig cmake pythonPackages.python ];
  buildInputs = [ stdenv pkgconfig libglvnd libGL libGLU libX11 opencascade freetype ];
  propagatedBuildInputs = with pythonPackages; [ six pyqt5 ];

  cmakeFlags = [
    "-DPYTHONOCC_INSTALL_DIRECTORY=${placeholder "out"}/${pythonPackages.python.sitePackages}/OCC"
  ];

  enableParallelBuilding = true;
})
