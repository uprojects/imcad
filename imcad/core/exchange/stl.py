"""STL data exchange
"""

from OCC.Core.TopoDS import TopoDS_Shape
from OCC.Core.StlAPI import StlAPI_Reader, StlAPI_Writer
from OCC.Core.BRepMesh import BRepMesh_IncrementalMesh

from .exchange import Reader, Writer, MeshOptions

class STLOptions(MeshOptions):
    """STL processor options"""

    def __init__(self, mode='text', linear_deflection=0.9, \
                 angular_deflection=0.5, deflection_relative=False):
        """Initialize STL processor options

        Arguments:
        mode -- the mode for writing file (either 'text' or 'binary')
        linear_deflection -- the linear deflection (lower provides \
        better quality but bigger file size)
        angular_deflection -- the angular deflection (lower provides \
        better quality but bigger file size)
        deflection_relative -- when True the linear deflection will \
        relate from edge length
        """
        self.mode = mode
        MeshOptions.__init__(self, linear_deflection, angular_deflection, deflection_relative)

class STL(Reader, Writer):
    """STL topology format support"""

    name = "STL"
    extensions = [".stl"]

    @staticmethod
    def impl_read_file(filename):
        shape = TopoDS_Shape()

        reader = StlAPI_Reader()

        reader.Read(shape, filename)

        return shape

    @staticmethod
    def impl_write_file(shape, filename, options):
        # meching the shape
        mesh = BRepMesh_IncrementalMesh(shape, options.linear_deflection, \
                                        options.deflection_relative, \
                                        options.angular_deflection, True)

        mesh.Perform()

        if not mesh.IsDone():
            raise AssertionError("Unable to generate mesh for shape")

        writer = StlAPI_Writer()

        if options.mode in ("text", "ascii"):
            writer.SetASCIIMode(True)
        else:
            writer.SetASCIIMode(False)

        writer.Write(shape, filename)

STL.register_reader()
STL.register_writer()
