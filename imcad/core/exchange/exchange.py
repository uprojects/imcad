"""Topology data formats support
"""

from os.path import splitext, isfile

class MeshOptions:
    """Mesh processor options"""

    def __init__(self, linear_deflection=0.9, \
                 angular_deflection=0.5, deflection_relative=False):
        """Initialize mesh processor options

        Arguments:
        linear_deflection -- the linear deflection (lower provides \
        better quality but bigger file size)
        angular_deflection -- the angular deflection (lower provides \
        better quality but bigger file size)
        deflection_relative -- when True the linear deflection will \
        relate from edge length
        """
        self.linear_deflection = linear_deflection
        self.angular_deflection = angular_deflection
        self.deflection_relative = deflection_relative

    def with_preset(self, preset):
        """Use specified quality preset ('low', 'medium', 'high')"""

        if preset in ('low', 'lo', 'lq', 'l'):
            self.linear_deflection = 0.9
            self.angular_deflection = 0.5
        elif preset in ('medium', 'me', 'mq', 'm'):
            self.linear_deflection = 0.1
            self.angular_deflection = 0.2
        elif preset in ('high', 'hi', 'hq', 'h'):
            self.linear_deflection = 0.001
            self.angular_deflection = 0.01
        else:
            raise AssertionError("Unknown mesh preset '%s'" % preset)

class Format:
    """File format support"""

    name = ""
    """The name of format"""

    extensions = []
    """The list of known file extensions"""

class Reader(Format):
    """Base class for topology format reader"""

    @staticmethod
    def impl_read_file(filename):
        """File reader implementation"""
        raise NotImplementedError()

    registry = []
    """Readers registry"""

    @classmethod
    def register_reader(cls):
        """Register reader"""
        Reader.registry.append(cls)

    @staticmethod
    def read_file(filename):
        """Read topology from file"""
        _, extension = splitext(filename)

        # find suitable reader by extension
        reader = _find_by_extension(Reader.registry, extension)

        if reader is None:
            raise NotImplementedError("Reader for file '%s' not implemented yet" % extension)

        if not isfile(filename):
            raise FileNotFoundError("Unable to read '%s' (not found)" % filename)

        shape = reader.impl_read_file(filename)

        if shape.IsNull():
            raise IOError("Error when reading file '%s'" % filename)

        return shape

class Writer(Format):
    """Base class for topology format writer"""

    @staticmethod
    def impl_write_file(shape, filename, options):
        """Write topology data to file"""
        raise NotImplementedError()

    registry = []
    """Writers registry"""

    @classmethod
    def register_writer(cls):
        """Register writer"""
        Writer.registry.append(cls)

    @staticmethod
    def match_writer(filename):
        """Find suitable writer by extension"""
        _, extension = splitext(filename)

        writer = _find_by_extension(Writer.registry, extension)

        return writer.name

    @staticmethod
    def write_file(shape, filename, options):
        """Write topology to file"""

        if shape.IsNull():
            raise AssertionError("Shape is null")

        #if isfile(filename):
        #    print("Warning: Because file '%s' already exists it will be overwritten" % filename)

        _, extension = splitext(filename)

        # find suitable writer by extension
        writer = _find_by_extension(Writer.registry, extension)

        if writer is None:
            raise NotImplementedError("Writer for file '%s' not implemented yet" % extension)

        writer.impl_write_file(shape, filename, options)

        if not isfile(filename):
            raise IOError("Unable to write file '%s'" % filename)

def _find_by_extension(registry, extension):
    for exchanger in registry:
        if extension in exchanger.extensions:
            return exchanger
    return None
