"""
Exporting tools
"""

from .exchange import Writer, STLOptions, STEPOptions, X3DOptions
from .context import BaseContext

class ExportContext(BaseContext):
    """Exporting context"""
    def __init__(self, filename, options):
        BaseContext.__init__(self)
        self.filename = filename
        self.options = options

    state = True
    """Status of export contexts"""

    @classmethod
    def enable(cls):
        """Enable export contexts"""
        cls.state = True

    @classmethod
    def disable(cls):
        """Disable export contexts"""
        cls.state = False

    def close_context(self):
        if not self.__class__.state:
            self.transfer_elements_to_parent()
            return

        self.assert_single_element("You able to export single element only")

        # get the shape of a last available element
        shape = self.last_element.shape

        # process export
        Writer.write_file(shape, self.filename, self.options)

        self.transfer_elements_to_parent()

def export(filename, schema='AP214IS', mode='ascii', preset='low', \
           linear_deflection=None, angular_deflection=None, deflection_relative=False):
    """Export result as a file (.step, .iges, .stl, .x3d are supported)

    Arguments:
    filename -- the name of file to export to
    schema -- the schema to setup STEP processor (default: 'AP214IS')
    mode -- the export mode for STL (one of: 'ascii', 'binary'. default: 'ascii')
    preset -- the preset for STL (one of: 'low', 'medium', 'high'. default: 'low')
    linear_deflection -- the linear deflection factor for STL (default: from preset)
    angular_deflection -- the angular deflection factor for STL (default: from preset)
    deflection_relative -- calculate the linear deflection relative to edge length
    """

    writer = Writer.match_writer(filename)
    options = None

    if writer in ('STL', 'X3D'):
        options = STLOptions(mode) if writer == 'STL' else X3DOptions()

        if preset is not None:
            options.with_preset(preset)
        if linear_deflection is not None:
            options.linear_deflection = linear_deflection
        if angular_deflection is not None:
            options.angular_deflection = angular_deflection
        if deflection_relative is not None:
            options.deflection_relative = deflection_relative
    elif writer == 'STEP':
        options = STEPOptions(schema)

    return ExportContext(filename, options)
