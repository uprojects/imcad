"""
Persistent caching decorator

Function-level caching solution which uses function info
and arguments to generate unique keys for topology data.

```
from imcad import cached, extrude, circle

@cached
def my_model(diameter=5, length=25, modules=5, offset=10):
    \"\"\"Cached model example\"\"\"
    for i in range(0, modules):
        with translate(x=offset * i), extrude(length):
            circle(d=diameter)
```
"""

from platform import system
from pickle import loads, dumps
from hashlib import sha256
from os import makedirs, getenv, getcwd, access, W_OK, remove
from os.path import isabs, isfile, isdir, join, getmtime
from inspect import getsource
from .context import current

def cached(fn):
    """Cached rendering decorator"""

    # use function info to invalidate cache when function changed
    fn_hash = _gen_hash(fn.__qualname__,
                        fn.__globals__['__name__'],
                        fn.__globals__['__file__'],
                        getsource(fn))

    def cached_fn(*args, **kwargs):
        # generate key
        key = _gen_hash(fn_hash, args, kwargs)

        # try to get from cache
        val = _cache_get(key)

        context = current()

        if val is None:
            initial_elements = len(context.elements)

            _cache_push()

            # generate data
            fn(*args, **kwargs)

            val = context.elements[initial_elements:]

            # and store it in cache
            _cache_set(key, val)

            _cache_pop()
        else:
            for element in val:
                context.put_element(element.clone())

    return cached_fn

def _sys_cache_dirs():
    sys = system()

    if sys in ('Linux', 'Unix') or sys.ends_with('BSD'):
        return [
            getenv('XDG_CACHE_HOME'),
            join(getenv('HOME'), '.cache'),
        ]
    if sys == 'Darwin':
        return [
            join(getenv('HOME'), 'Library', 'Caches')
        ]
    if sys == 'Windows':
        return [
            getenv('LOCALAPPDATA'),
            join(getenv('USERPROFILE'), 'AppData', 'Local')
        ]
    return []

def _init_dir():
    path = getenv("IMCAD_CACHE_DIR")

    if path:
        if not isabs(path):
            path = join(getcwd(), path)
        if not isdir(path):
            # try to create dir when it hasn't exists
            try:
                makedirs(path)
            except OSError:
                raise RuntimeError("Unable to create cache directory '%s' \
given from environment variable 'IMCAD_CACHE_DIR'" % path)
            print("Created cache directory '%s'" % path)
        if not access(path, W_OK):
            raise RuntimeError("Unable to store caches at '%s' \
given from environment variable 'IMCAD_CACHE_DIR'" % path)
        return path

    paths = _sys_cache_dirs()

    for path in paths:
        if path and access(path, W_OK):
            path = join(path, 'imcad')
            if not isdir(path):
                # try to create dir when it hasn't exists
                try:
                    makedirs(path)
                except OSError:
                    raise RuntimeError("Unable to create cache directory '%s'" % path)
                print("Created cache directory '%s'" % path)
            return path

    print("Unable to initialize cache directory. The disk cache unavailabe.")
    return None

def _gen_hash(*args):
    key = dumps(args)

    sha = sha256()
    sha.update(key)

    return sha.hexdigest()

class Validator:
    """Base cache validator"""

    def __init__(self):
        self.result = None

    def validate(self):
        """Validation method"""
        raise NotImplementedError()

    def revalidate(self):
        """Revalidate previous validation"""
        return self.validate() == self.result

class FileMTime(Validator):
    """Validate cache using file mtime"""

    def __init__(self, filename):
        Validator.__init__(self)
        self.filename = filename

    def validate(self):
        self.result = getmtime(self.filename)

def _revalidate_all(validations):
    for validation in validations:
        if not validation.revalidate():
            return False
    return True

_STACK_VALIDATIONS = []

def put_validation(validation):
    """Add extra validator for cache"""

    # run first validation
    validation.validate()

    # store validator with args and result to get it possible re-validation
    for validators in _STACK_VALIDATIONS:
        validators.append(validation)

def _cache_push():
    _STACK_VALIDATIONS.append([])

def _cache_pop():
    _STACK_VALIDATIONS.pop()

def _cache_get(key):
    # try to get from RAM cache first
    entry = _ram_get(key)

    if entry is not None:
        if not _revalidate_all(entry[1]):
            _ram_del(key)
            return None
        return entry[0]

    # try to get from disk cache
    entry = _fs_get(key)

    if entry is not None:
        if not _revalidate_all(entry[1]):
            _fs_del(key)
            return None
        _ram_set(key, entry)
        return entry[0]

    return None

def _cache_set(key, val):
    # append validations for cache
    entry = (val, _STACK_VALIDATIONS[-1])

    # store in RAM cache
    _ram_set(key, entry)
    # store in disk cache
    _fs_set(key, entry)

_RAM_CACHE = {}

def _ram_get(key):
    if key in _RAM_CACHE:
        return _RAM_CACHE[key]
    return None

def _ram_set(key, val):
    _RAM_CACHE[key] = val

def _ram_del(key):
    del _RAM_CACHE[key]

_DIR_NAME = _init_dir()

def _gen_path(key):
    if _DIR_NAME:
        return join(_DIR_NAME, key + '.brep')
    return None

def _fs_get(key):
    path = _gen_path(key)

    if not path or not isfile(path):
        return None

    try:
        with open(path, "rb") as fd:
            return loads(fd.read())
    except:
        # remove invalid cached data
        remove(path)
        return None

def _fs_set(key, val):
    path = _gen_path(key)

    if not path:
        return

    with open(path, "wb") as fd:
        fd.write(dumps(val))

def _fs_del(key):
    path = _gen_path(key)

    if path and isfile(path):
        remove(path)
