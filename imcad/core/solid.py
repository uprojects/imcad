"""
Solid tools
"""

from math import pi as PI
from OCC.Core.gp import gp, gp_Vec
from OCC.Core.BRepPrimAPI import BRepPrimAPI_MakePrism, BRepPrimAPI_MakeRevol
from OCC.Core.BRepBuilderAPI import BRepBuilderAPI_Transform

from .context import BaseContext, BaseElement, current
from .sketch import SketchContext
from .helpers import dumps, loads

def assert_in_solid():
    """Check in sketching context"""
    BaseContext.current.assert_in_context(SolidContext, "Out of solid")

class SolidContext(BaseContext):
    """Base class for solid contexts"""

    def __init__(self):
        BaseContext.__init__(self)

    def close_context(self):
        self.assert_some_elements("Empty solid")
        shape = self.process_solid()
        current().put_element(SolidElement(shape))

    def process_solid(self):
        """Processing function"""
        raise NotImplementedError()

class SolidElement(BaseElement):
    """Base class for solid elements"""

    def __init__(self, shape):
        BaseElement.__init__(self)
        self.shape = shape

    def transform(self, trsf):
        shape = BRepBuilderAPI_Transform(trsf)
        shape.Perform(self.shape)
        return self.__class__(shape.Shape())

    def clone(self):
        return self.__class__(self.shape)

    def dumps(self):
        return dumps(self.shape)

    def loads(self, raw_data):
        self.shape = loads(raw_data)

class ExtrudeContext(SketchContext):
    """Linear extrusion context"""

    def __init__(self, l):
        SketchContext.__init__(self)
        self.length = l

    def close_context(self):
        face = self.process_sketch()

        prism = BRepPrimAPI_MakePrism(face, gp_Vec(0.0, 0.0, self.length))
        shape = prism.Shape()

        current().put_element(SolidElement(shape))

def extrude(l=0.0):
    """Extrude face"""
    return ExtrudeContext(l)

class RevolveContext(SketchContext):
    """Revolving context"""

    def __init__(self, a):
        SketchContext.__init__(self)
        self.angle = a

    def close_context(self):
        face = self.process_sketch()

        prism = BRepPrimAPI_MakeRevol(face, gp.OY(), self.angle)
        shape = prism.Shape()

        current().put_element(SolidElement(shape))

def revolve(a=360.0):
    """Revolve face"""
    return RevolveContext(a*PI*2/360.0)
