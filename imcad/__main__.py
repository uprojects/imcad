from argparse import ArgumentParser
from sys import stdin, stdout

from .core.service import Service

parser = ArgumentParser(description='IMCAD -- Immediate-mode CAD tool')

#parser.add_argument('--host', '-H',
#                    default='localhost',
#                    nargs='?',
#                    type=str,
#                    help='Host for server')
#parser.add_argument('--port', '-P',
#                    default=3344,
#                    nargs='?',
#                    type=int,
#                    help='Port for server')

args = parser.parse_args()

Service(stdin, stdout).run()
